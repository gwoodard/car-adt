/*

George Woodard
CS 215 Fall 2014
Programming Assignment #4
 
This is the Car ADT specification

*/

#include <string>
using namespace std;

class Car
{

private:
	int year;
	string color;
	string make;
	string model;
	int highwayMPG;
	int cityMPG;
	int miles;
	string changeOil;
	string accidents;


public:
	bool oilChange(int lifeOfMiles);
	string getOilStatus();
	int getCarYear();
	string getCarMake();
	string getCarModel();
	string getCarColor();
	int getMilesHgwy();
	int getMilesCity();
	int getCarMiles();
	string carFaxReport();
	Car::Car();
	Car::Car(int carYear, string carColor, string carMake, string carModel, int mpgHighway, int mpgCity, int carMiles, string changeOfOil, string carFax);


};

//End of Car Class