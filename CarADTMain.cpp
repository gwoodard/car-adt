/*

George Woodard
CS 215 Fall 2014
 Programming Assignment #4


This is the Car ADT main program

*/

#include <iostream>
#include <string>
#include "Car.h"
using namespace std;

int main()
{
	//start of main program
	//object instantiation
	
	Car car1(2012, "Black", "Toyota", "Highlander", 28, 20, 45000, "change oil", "No Accidents");

	//get car Year, Make Model
	cout<<"The car available for sale is a "<<car1.getCarYear()<<" "<<car1.getCarMake()<<" "<<car1.getCarModel()<<endl;
	//get car Color
	cout<<"The color of the car is "<<car1.getCarColor()<<endl;
	//get car miles per gallon in Highway/City format
	cout<<"The Highway and City miles per gallon (in Hghwy/City format) is "<<car1.getMilesHgwy()<<"/"<<car1.getMilesCity()<<endl;
	//get car Mileage
	cout<<"The total miles of the car is "<<car1.getCarMiles()<<endl;
	//get car oil change status
	cout<<"Current oil change status is "<<car1.getOilStatus()<<endl;
	//get car accident status
	cout<<"The current car Fax for the "<<car1.getCarModel()<<" is "<<car1.carFaxReport()<<endl;
	cout<<endl;
	system("pause");

}