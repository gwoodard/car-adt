# Car Abstract Data Type Assignment #

### **About:** ###

Repo containing program on Abstract Data Types. This program allows you to declare an array call 'Car' with an object called 'car1'. In each object in the array you declare, in this order:
- Year
- Color
- Make
- Model
- Highway and City Miles Per Gallon
- Current Car Mileage
- Car Oil Change Status
- Car Condition

Once all info is declared the output is in a 'Car Fax' style.

### **IDE:**
Visual Studio

### **Input:** ###
None / Declared in main file

### **Language:** ###
C++