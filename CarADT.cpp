/*

George Woodard
CS 215 Fall 2014
Programmming Assignment #4
 
This is the Car ADT implementation/definition

*/

#include "Car.h"



bool Car::oilChange(int lifeOfMiles)//beginning of oil change method
{
	bool result = false;
	miles = lifeOfMiles;

	if(lifeOfMiles <=0)
	{
		result = false;
	}
	else
	{
		if (lifeOfMiles > 3000)
		{
			changeOil = "Time for an oil change";
		}
		else
		{
			changeOil = "Not ready for an oil change";
		}
		result = true;
	}
	return result;
}//end of oil change method

string Car::getOilStatus(){
	return changeOil;
}

int Car::getCarYear(){
	return year;
}


string Car::getCarMake(){

	return make;
}

string Car::getCarModel(){
	return model;
}

string Car::getCarColor(){
	return color;
}

int Car::getMilesHgwy(){
	return highwayMPG;
}

int Car::getMilesCity(){
	return cityMPG;
}

int Car::getCarMiles(){
	return miles;
}





string Car::carFaxReport(){
	return accidents;
}

Car::Car()
{
	
}


Car::Car(int carYear, string carColor, string carMake, string carModel, int mpgHighway, int mpgCity, int carMiles, string changeOfOil, string carFax)
{
	year = carYear;
	color = carColor;
	make = carMake;
	model = carModel;
	highwayMPG = mpgHighway;
	cityMPG = mpgCity;
	miles = carMiles;
	changeOil = changeOfOil;
	accidents = carFax;
}
